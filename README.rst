****************************
LHCb Online Docker container
****************************

Prerequisites
=============

You will need Docker and CVMFS installed and configured.

Docker
------

The instructions for installing the official release of the Docker engine are here: https://docs.docker.com/engine/install/

On Debian and Ubuntu, Docker is also packaged as part of the distribution.

Red Hat developed a drop-in Docker replacement called Podman. On CentOS 7, it is distributed as part of the ``extras`` repository, which is enabled by default. On CentOS 8, it is part of the ``container-tools`` module.

CVMFS
-----

The instructions for installing and configuring CVMFS are here: https://cvmfs.readthedocs.io/en/stable/cpt-quickstart.html

In that page, the instructions in the "Create default.local" section are a bit unclear. In practice, you'll need to specify exactly one parameter in ``/etc/cvmfs/default.local``:  ``CVMFS_HTTP_PROXY``.

If you are installing CVMFS on a computer on the CERN network, use::

    CVMFS_HTTP_PROXY='http://ca-proxy.cern.ch:3128'

If you are installing CVMFS on a computer on the LHCb network, use::

    CVMFS_HTTP_PROXY='http://cvmfsproxy01.lbdaq.cern.ch:3128|http://cvmfsproxy02.lbdaq.cern.ch:3128|http://cvmfsproxy03.lbdaq.cern.ch:3128'

If you’re unsure about the proxy names, set::

    CVMFS_HTTP_PROXY=DIRECT

If the auto-mounter is configured correctly, there is no need to set ``CVMFS_REPOSITORIES``.

Usage
=====

Start a container (named ``my_lhcbonline`` here) using the helper script provided by this repository::

    ./lhcb-online-container start my_lhcbonline

Depending on how your Docker installation is configured, you might have to run the helper script as root.

The helper script takes care of mounting the ``/cvmfs`` and ``/home`` directories inside the container.

Run a Bash shell in the container::

    ./lhcb-online-container enter my_lhcbonline

If you run the helper script as root, the command above will give you a root shell inside the container. To get a shell for your user run::

    ./lhcb-online-container enter my_lhcbonline $(whoami)

Once you are done playing with the container, destroy it with::

    ./lhcb-online-container stop my_lhcbonline

Before destroying the container, make sure that you saved your data outside of it (for example in your home directory, which is mounted inside the container by the helper script)! No changes to the container files are persisted.

Getting started with LHCb/Online development
============================================

From the container shell, initialize your LHCb environment::

    export CMTCONFIG=x86_64-centos7-gcc9-dbg
    source /cvmfs/lhcb.cern.ch/group_login.sh

Clone the LHCb/Online project::

    git clone https://:@gitlab.cern.ch:8443/lhcb/Online.git

Initialize the project::

    cd Online
    lb-project-init

Pick a version of the LHCb/LHCb project to depend on and set it in the ``CMakelists.txt``. For example, to declare a dependency on LHCb v51r0::

    sed -i 's/USE\s\+LHCb\s\+\w\+/USE LHCb v51r0/' CMakeLists.txt

Build the Online project::

    make