# Base image
FROM centos:7

# Add the OS dependencies of LHCb/Online and cleanup unused files
RUN \
yum -y install gcc glibc-devel glibc-static libaio-devel make motif-devel numactl-devel openssl-devel redhat-lsb-core xz-devel && \
yum -y clean all && \
rm -rf /var/cache/yum/x86_64 && \
rm -rf /usr/lib/udev/hwdb.d/* && \
rm -f /etc/udev/hwdb.bin

# Run bash within the container.
CMD [ "/bin/bash" ]
